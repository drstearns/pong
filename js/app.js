/* script file for the Canvas demo */

document.addEventListener('DOMContentLoaded', function() {
    'use strict';

    //reference to the canvas element
    var canvas = document.getElementById('game-canvas');

    //get the 2D drawing context
    var ctx = canvas.getContext('2d');

    //set default font for text messages
    var fontSize = 20;
    ctx.font = fontSize + 'px Helvetica';

    //the current game state
    var gameState;

    function newGameState() {
        return {
            paddle: {
                left: 20,
                top: 0,
                width: 10,
                height: canvas.height / 6,
                bottom: function() {return this.top + this.height},
                right: function() {return this.left + this.width}
            },
            ball: {
                x: 35,                              //x center point
                y: Math.random() * canvas.height,   //y center point
                radius: 5,                          //radius of the circle
                velocity: 5,
                vectorX: 1,
                vectorY: 1,
                left: function() {return this.x - this.radius},
                top: function() {return this.y - this.radius},
                bottom: function() {return this.y + this.radius},
                right: function() {return this.x + this.radius}                
            },
            lastTimestamp: performance.now()
        };
    } //newGameState()

    function renderMessage(message) {
        var metrics = ctx.measureText(message);
        ctx.fillText(message, (canvas.width - metrics.width) / 2, (canvas.height - 18) / 2);
    }

    //render the game state
    function render() {
        //clear the entire canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        //draw the paddle
        var paddle = gameState.paddle;
        ctx.fillRect(paddle.left, paddle.top, paddle.width, paddle.height);

        //draw the ball
        var ball = gameState.ball;
        ctx.beginPath();
        ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI);
        ctx.fill();
    } //render()

    function step() {
        var ball = gameState.ball;
        var paddle = gameState.paddle;

        //move the ball
        ball.x += ball.vectorX * ball.velocity;
        ball.y += ball.vectorY * ball.velocity;

        //detect collisions
        //right wall
        if (ball.right() >= canvas.width) {
            ball.vectorX = -ball.vectorX;
        }

        //top & bottom walls
        if (ball.top() <= 0 || ball.bottom() >= canvas.height) {
            ball.vectorY = -ball.vectorY;
        }

        //paddle
        if (ball.left() <= paddle.right()) {
            //if it's in the range of the paddle, treat as collision
            if(ball.bottom() >= paddle.top && ball.top() <= paddle.bottom()) {
                ball.vectorX = -ball.vectorX;
            }
            else {
                //game over
                return false;
            }
        }

        return true;
    } //step()

    function animate(timestamp) {
        render();
        var keepGoing = true;
        if (timestamp - gameState.lastTimestamp >= 16) {
            gameState.lastTimestamp = timestamp;
            keepGoing = step();
        }

        if (keepGoing) {
            requestAnimationFrame(animate);
        }
        else {
            renderMessage('Game Over!');
        }

    } //animate()

    function startGame() {
        //create a new game state
        gameState = newGameState();

        requestAnimationFrame(animate)
    } //startGame()

    //move the paddle corresponding to the mouse position
    document.addEventListener('mousemove', function(evt) {
        var canvasY = evt.clientY - canvas.offsetTop;
        gameState.paddle.top = canvasY - (gameState.paddle.height / 2);
    });

    document.addEventListener('keydown', function(evt) {
        var key = evt.key || evt.keyCode;
        if (32 === key && undefined === gameState.timer) {
            startGame();
        }
    });

    startGame();

});
